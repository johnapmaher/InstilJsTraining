Collection of js exercises completed during training in Instil Belfast

repl.html -> REPL developed using ace.c9.io

fizzbuzz.html -> incomplete fizzbuzz solution

ooJS.html -> demonstration of Object oriented javascript including inheritance

portMgmt -> incomplete portMgmt exercise

functional.js -> demonstration of functional features of javascript

portMgmtSolution.html -> solution to portMgmt with some use of functional 
features