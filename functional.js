//functional forEach
function printList(list) {
	list.forEach(function(item) {
		console.log(item);
	});
}
//functional map
function double(list) {
	return list.map(function(item) {
		return item*2;
	});
}
//functional filter
function getEven(list) {
	return list.filter(function(item) {
		return item % 2 === 0;
	});
}

function sum(list) {
	return list.reduce(function(x, y) {
		return x + y;
	}, 100);
}

var l = [1,2,3,4,5,6,7,8];

console.log(sum(getEven(l)));

